## Void Dotfiles

Dotfiles for my Void Linux setup with BSPWM. You should have the following installed: 

<img src="screenshot1.png" width=400>
<img src="screenshot2.png" width=400>

### Applications
- BSPWM (Window Manager)
- Polybar (Toolbar)
- sxhkd (Keybindings)
- picom (Compositor)
- uxvrt (Terminal emulator)
- Betterlockscreen (minimal screenlock)
- Bottom (system monitor)
- Ranger (file manager)
- Spotify
- Spicetify (CLI tools for Spotify theming)
- Cava (Audio visualizer)
- Dunst (notification manager)

### Fonts
- Ioveska
- Material Design

### Notes
- BSPWM is set up for my personal setup and application usage. Modify at your leisure.
- Polybar includes scripts to view currently playing Spotify, display ZFS pool allocation/pool size, and to check if my Mullvad Wireguard connection is up or not. Modify at your leisure.
